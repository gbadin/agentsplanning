package fr.Agent.Smith;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Planning implements Serializable {

    public static final String[] LIST_SYMBOLE_PLANNING = {"J", "N", "/J", "/N", "-", "i", "I", "v"};

    private transient List<Semaine> planning;

    public Planning() {
        planning = new ArrayList<Semaine>(Main.NB_SEMAINES);
        for (int i = 0; i < Main.NB_SEMAINES; i++) {
            planning.add(new Semaine(Main.NB_JOURS_PAR_SEMAINES));
        }
    }

    public int getNbJoursTotal() {
        return Main.NB_SEMAINES * Main.NB_JOURS_PAR_SEMAINES;
    }

    public int getNbSemainesTotal() {
        return Main.NB_SEMAINES;
    }

    public int getNbDaysPerWeek() {
        return Main.NB_JOURS_PAR_SEMAINES;
    }

    public Semaine getSemaine(int idx) {
        return planning.get(idx);
    }

    public Jour getJour(int idx)
    {
        return planning.get((int)Math.floor(idx/Main.NB_JOURS_PAR_SEMAINES)).getJour(idx%Main.NB_JOURS_PAR_SEMAINES);
    }
    

    @Override
    public String toString() {
        String res = "Planning : \n";

        for (Semaine sem : planning) {
            res += sem + "\n";
        }
        return res;
    }

    //calcule si l'agent peut travailler le jour indiqué
    public boolean canWorkDay(int idxJour, Agent a)
    {
        Jour j = getJour(idxJour);
        if(!(a.testGuardeSuite(idxJour)
        && a.getCptGardeSemaine(idxJour/Main.NB_JOURS_PAR_SEMAINES) < 4   // pas plus de 4 gardes par semaine
        && !j.getNuit().getListAgents().contains(a)) // l'agent ne fait pas la nuit suivante
        && (idxJour == 0 || !getJour(idxJour-1).getNuit().getListAgents().contains(a)))// l'agent ne fait pas la nuit precedente
        {
            return false;
        }
        if(a.getType().equals("AS"))
        {
            for(Agent agt : j.getJournee().getListAgents())
            {
                if(agt.getType().equals("AS"))//il ne peut y avoir qu'un AS
                    return false;
            }
        }
        else
        {
            int count = 0;
            for(Agent agt : j.getJournee().getListAgents())
            {
                if(agt.getType().equals("PM"))//il ne peut y avoir que 3 PM
                    count ++;
            }
            if(count > 2)
                return false;
        }
        return true;
    }

    //calcule si l'agent peut travailler la nuit indiqué
    public boolean canWorkNight(int idxJour, Agent a)
    {
        Jour j = getJour(idxJour);
        if(!(a.testGuardeSuite(idxJour)
                && a.getCptGardeSemaine(idxJour/Main.NB_JOURS_PAR_SEMAINES) < 4   // pas plus de 4 gardes par semaine
                && !j.getJournee().getListAgents().contains(a)) // l'agent ne fait pas la nuit précédente
                && (idxJour+1 >= Main.NB_JOURS_PAR_SEMAINES*Main.NB_SEMAINES || !getJour(idxJour+1).getNuit().getListAgents().contains(a))) // l'agent ne fait pas la journée suivante
        {
            return false;
        }
        for(Agent agt : j.getNuit().getListAgents())
        {
            if(agt.getType().equals(a.getType()))//il ne peut y avoir qu'un AS ou PM
                return false;
        }
        return true;
    }

    ///////////////
    //SERIALIZATION
    ///////////////
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeInt(planning.size());
        for (Semaine semaine : planning) {
            s.writeObject(semaine);
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        int size = s.readInt();
        planning = new ArrayList<Semaine>();
        for (int i = 0; i < size; i++) {
            Semaine semaine = (Semaine) s.readObject();
            planning.add(semaine);
        }
    }
}
