package fr.Agent.Smith;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MessagePlanningContrainteAgent extends Message {

    public static final String type = "MessagePlanningContrainte";

    private transient Agent agent;

    public MessagePlanningContrainteAgent(Agent agent) {
        this.agent = agent;
    }

    @Override
    public String getType() {
        return type;
    }

    public Agent getAgent() {
        return agent;
    }

    ///////////////
    //SERIALIZATION
    ///////////////
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeObject(agent.getName());
        s.writeObject(agent.getType());
        s.writeInt(agent.getListContraintes().size());
        for (String contrainte : agent.getListContraintes()) {
            s.writeObject(contrainte);
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        String name = (String) s.readObject();
        String type = (String) s.readObject();
        int size = s.readInt();
        List<String> contraintes = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            contraintes.add((String) s.readObject());
        }
        if (Environment.getEnvironment().containsAgent(name)) {
            Agent a = Environment.getEnvironment().getAgent(name);
            a.setListContraintes(contraintes);
            this.agent = a;
        } else {
            this.agent = new Agent(name, type, contraintes);
        }
    }
}
