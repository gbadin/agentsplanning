package fr.Agent.Smith;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MessageAskForResolveConflict extends Message {

    public static final String type = "MessageAskForResolveConflict";

    @Override
    public String getType() {
        return type;
    }

    private Planning planning;
    private transient List<Integer> conflictsDays;
    private int[] cptGardeSuite;
    private int[] cptGardeSemaine;
    private String agentName;

    public MessageAskForResolveConflict(Planning planning, List<Integer> conflictsDays, int[] cptGardeSuite, int[] cptGardeSemaine, String agentName) {
        this.planning = planning;
        this.conflictsDays = conflictsDays;
        this.cptGardeSuite = cptGardeSuite;
        this.cptGardeSemaine = cptGardeSemaine;
        this.agentName = agentName;
    }

    public Planning getPlanning() {
        return planning;
    }

    public void setPlanning(Planning planning) {
        this.planning = planning;
    }

    public List<Integer> getConflictsDays() {
        return conflictsDays;
    }

    public void setConflictsDays(List<Integer> conflictsDays) {
        this.conflictsDays = conflictsDays;
    }

    public int[] getCptGardeSuite() {
        return cptGardeSuite;
    }

    public void setCptGardeSuite(int[] cptGardeSuite) {
        this.cptGardeSuite = cptGardeSuite;
    }

    public int[] getCptGardeSemaine() {
        return cptGardeSemaine;
    }

    public void setCptGardeSemaine(int[] cptGardeSemaine) {
        this.cptGardeSemaine = cptGardeSemaine;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    ///////////////
    //SERIALIZATION
    ///////////////
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        s.writeInt(conflictsDays.size());
        for (Integer i : conflictsDays) {
            s.writeObject(i);
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        int size = s.readInt();
        conflictsDays = new ArrayList<Integer>(size);
        for (int i = 0; i < size; i++) {
            conflictsDays.add((Integer) s.readObject());
        }
    }

}
