package fr.Agent.Smith;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.QueueingConsumer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class RabbitMQListenerThread extends Thread {

    private QueueingConsumer consumer;
    private final String topic;
    private final IRabbitCallback callback;

    public RabbitMQListenerThread(RabbitMQServer server, String topic, IRabbitCallback callback) {
        super();

        this.topic = topic;
        this.callback = callback;
        try {
            Connection connection = server.getConnection();
            Channel channel = connection.createChannel();
            consumer = new QueueingConsumer(channel);
            channel.basicConsume(topic, true, consumer);
        } catch (IOException e) {
            System.out.println("Unable to listen Topic : " + topic + ". Error : " + e.getMessage());
        }
    }

    @Override
    public void run() {
        super.run();

        boolean running = true;
        while (running) {
            QueueingConsumer.Delivery delivery;
            try {
                delivery = consumer.nextDelivery();
                ByteArrayInputStream bais = new ByteArrayInputStream(delivery.getBody());
                ObjectInputStream ois = new ObjectInputStream(bais);
                Message message = (Message) ois.readObject();
                ois.close();

                callback.messageReceived(topic, message);
            } catch (InterruptedException e) {
                System.out.println("Unable to get message. Error : " + e.getMessage());
                running = false;
            } catch (ClassNotFoundException e) {
                System.out.println("Unable to deserialize message. Error : " + e.getMessage());
            } catch (IOException e) {
                System.out.println("Unable to deserialize message. Error : " + e.getMessage());
            }
        }
    }
}
