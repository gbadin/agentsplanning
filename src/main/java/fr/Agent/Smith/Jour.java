package fr.Agent.Smith;

import java.io.Serializable;

public class Jour implements Serializable {

    private int numJour; // numéro du jour de la semaine (1..7)
    private Journee j;
    private Nuit n;

    public Jour() {
        j = new Journee();
        n = new Nuit();
    }

    public void setNumJour(int numJour) {
        this.numJour = numJour;
    }

    public int getNumJour() {
        return numJour;
    }

    public Journee getJournee() {
        return j;
    }

    public Nuit getNuit() {
        return n;
    }
}
