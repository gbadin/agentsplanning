package fr.Agent.Smith;

import java.util.ArrayList;
import java.util.List;

public class RabbitMQListener {

    private final List<Thread> threads;

    public RabbitMQListener(RabbitMQServer server, ResponsablePlanning responsable, String[] topics) {
        threads = new ArrayList<Thread>();
        for (String t : topics) {
            RabbitMQListenerThread thread = new RabbitMQListenerThread(server, t, new ResponsableRabbitCallback(responsable));
            threads.add(thread);
        }
    }

    public void start() {
        for (Thread t : threads) {
            t.start();
        }
    }

    public void stop() {
        for (Thread t : threads) {
            t.interrupt();
        }
    }
}
