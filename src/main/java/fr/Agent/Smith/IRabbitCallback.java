package fr.Agent.Smith;

public interface IRabbitCallback {
    void messageReceived(String topic, Message message);
}
