package fr.Agent.Smith;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Semaine implements Serializable {

    private transient List<Jour> listJours;

    public Semaine(int nbJours) {
        listJours = new ArrayList<Jour>(nbJours);
        for (int i = 0; i < nbJours; i++) {
            Jour j = new Jour();
            j.setNumJour(i + 1);
            listJours.add(j);
        }
    }

    public Jour getJour(int idx) {
        return listJours.get(idx);
    }

    @Override
    public String toString() {

        String res = " ";
        for (int i = 0; i < listJours.size(); i++) {
            res += "____________________ ";
        }
        res += "\n|";

        for (Jour j : listJours) {
            String tmp = "" + j.getJournee();
            res += tmp;
            for (int i = 0; i < 20 - tmp.length(); i++)
                res += " ";
            res += "|";
        }

        res += "\n|";
        for (int i = 0; i < listJours.size(); i++) {
            res += "____________________|";
        }

        res += "\n|";
        for (Jour j : listJours) {
            String tmp = "" + j.getNuit();
            res += tmp;
            for (int i = 0; i < 20 - tmp.length(); i++)
                res += " ";
            res += "|";
        }

        res += "\n|";
        for (int i = 0; i < listJours.size(); i++) {
            res += "____________________|";
        }

        return res;
    }

    ///////////////
    //SERIALIZATION
    ///////////////
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeInt(listJours.size());
        for (Jour jour : listJours) {
            s.writeObject(jour);
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        int size = s.readInt();
        listJours = new ArrayList<Jour>();
        for (int i = 0; i < size; i++) {
            Jour jour = (Jour) s.readObject();
            listJours.add(jour);
        }
    }
}
