package fr.Agent.Smith;

import java.io.Serializable;

public abstract class Message implements Serializable {
    public abstract String getType();
}
