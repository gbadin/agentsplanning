package fr.Agent.Smith;

public class MessageResponseResolveConflict extends Message {

    public static final String type = "MessageAskForResolveConflict";

    @Override
    public String getType() {
        return type;
    }

    private String agentName;
    private String agentType;
    private int dayAdded;
    private boolean canResolve;

    public MessageResponseResolveConflict(String agentName, String agentType, int dayAdded, boolean canResolve) {
        this.agentName = agentName;
        this.agentType = agentType;
        this.dayAdded = dayAdded;
        this.canResolve = canResolve;
    }

    public MessageResponseResolveConflict(String agentName, String agentType, int dayAdded) {
        this(agentName,agentType,dayAdded,true);
    }

    public MessageResponseResolveConflict(String agentName, String agentType) {
        this(agentName,agentType,0,false);
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public int getDayAdded() {
        return dayAdded;
    }

    public void setDayAdded(int dayAdded) {
        this.dayAdded = dayAdded;
    }

    public boolean canResolve() {
        return canResolve;
    }

    public void setCanResolve(boolean canResolve) {
        this.canResolve = canResolve;
    }
}
