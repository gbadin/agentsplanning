package fr.Agent.Smith;

import java.util.HashMap;
import java.util.Map;

public class Environment {

    private static Environment instance;

    public static Environment getEnvironment() {
        if (instance == null) {
            instance = new Environment();
        }
        return instance;
    }

    private Map<String, Agent> agents;

    private Environment() {
        agents = new HashMap<String, Agent>();
    }

    public Agent putAgent(String name, Agent agent) {
        return agents.put(name, agent);
    }

    public Agent getAgent(String name) {
        return agents.get(name);
    }

    public boolean isEmpty() {
        return agents.isEmpty();
    }

    public boolean containsAgent(String name) {
        return agents.containsKey(name);
    }

    public boolean containsAgent(Agent agent) {
        return agents.containsValue(agent);
    }

    public int size() {
        return agents.size();
    }

    public Agent remove(String name) {
        return agents.remove(name);
    }
}
