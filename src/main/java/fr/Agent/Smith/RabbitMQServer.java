package fr.Agent.Smith;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class RabbitMQServer {

    private Channel channel;
    private Connection connection;

    public static final String DEFAULT_HOST = "localhost";
    public static final String[] QUEUES_NAME = {"AS", "PM"};

    public RabbitMQServer() {
        this(DEFAULT_HOST);
    }

    public RabbitMQServer(String host) {
        super();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();

            for (String s : QUEUES_NAME) {
                channel.queueDeclare(s, false, false, false, null);
            }
        } catch (IOException e) {
            System.out.println("Unable to connect to server. Error : " + e.getMessage());
        }
    }

    public void addQueue(String queueName) {
        try {
            channel.queueDeclare(queueName, false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void SendMessage(String topic, String message) {
        try {
            channel.basicPublish("", topic, null, message.getBytes());
            System.out.println(" [x] Sent '" + message + "' on topic : " + topic);
        } catch (IOException e) {
            System.out.println("Unable to send message : " + message + " on topic : " + topic + ". Error : " + e.getMessage());
        }
    }

    public void SendMessage(String topic, Message message) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(message);
            oos.close();

            channel.basicPublish("", topic, null, baos.toByteArray());
        } catch (IOException e) {
            System.out.println("Unable to send message : " + message + " on topic : " + topic + ". Error : " + e.getMessage());
        }
    }

    public void close() {
        try {
            channel.close();
            connection.close();
        } catch (IOException e) {
            System.out.println("Unable to close connection to server. Error : " + e.getMessage());
        }
    }

    public Channel getChannel() {
        return channel;
    }

    public Connection getConnection() {
        return connection;
    }
}
