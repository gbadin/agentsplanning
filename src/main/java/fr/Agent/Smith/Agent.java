package fr.Agent.Smith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Agent {

    private String name;
    private String type;//PM ou AS
    private int numberOfHourWorked;
    private transient List<String> listContraintes; // liste des contraintes à "respecter"
    private transient RabbitMQServer server;
    private transient RabbitMQListenerThread listener;

    private int[] cptGardeSuite; // nombre de garde effectuer à la suite par semaines
    private int[] cptGardeSemaine; // nombre de garde en 1 semaine par semaines

    /* Explications symboles contraintes
        *  J   : veut travailler la journee
        *  N   : veut travailler la nuit
        *  /J  : ne veut pas travailler la journée
        *  /N  : ne veut pas travailler la nuit
        *  -   : pas de contraintes particulieres
        *  i   : ne veut pas travailler (peut etre remis en cause)
        *  I   : ne veut pas travailler
        *
        *  /w  : pas le week end
        *  /m  : pas le mercredi
        *  /jm : pas la journee de mercredi
        **/
    private final String[] listSymboleContraintes = {"J", "N", "/J", "/N", "-", "i", "I"};
    //private String[] listPreferencesPossibles = {"/w", "/m", "/jm", "/2g"};


    //////////////
    //CONSTRUCTORS
    //////////////
    private void commonInit(String name, String type, List<String> listContraintes, int nbJoursPlanning) {

        numberOfHourWorked = 0;

        this.name = name;
        if (type.equals("AS") || type.equals("PM"))
            this.type = type;
        this.listContraintes = listContraintes;

        if (nbJoursPlanning > 0) {
            cptGardeSuite = new int[nbJoursPlanning];
            cptGardeSemaine = new int[nbJoursPlanning / Main.NB_JOURS_PAR_SEMAINES];
            for (int i = 0; i < nbJoursPlanning / Main.NB_JOURS_PAR_SEMAINES; i++) {
                cptGardeSemaine[i] = 0;
            }
            for (int i = 0; i < nbJoursPlanning; i++) {
                cptGardeSuite[i] = 0;
            }


            if (listContraintes.isEmpty()) {
                //init contraintes
                for (int i = 0; i < nbJoursPlanning; i++) {
                    listContraintes.add(listSymboleContraintes[(int) (Math.random() * listSymboleContraintes.length)]);
                }
            }
        }

        if (!Environment.getEnvironment().containsAgent(name)) {
            Environment.getEnvironment().putAgent(name, this);
        } else { //if the agent already exists, we change the value (should never do that might create bugs)

            System.err.println("Warning : Trying to create an agent that already exists, Copying values, this might cause bugs");
            Agent a = Environment.getEnvironment().getAgent(name);
            a.setName(name);
            a.setType(type);
            a.setListContraintes(listContraintes);
            if (nbJoursPlanning > 0) {
                a.cptGardeSemaine = cptGardeSemaine;
                a.cptGardeSuite = cptGardeSuite;
            }
        }
    }

    public Agent(String name, String type) {
        commonInit(name, type, new ArrayList<String>(), 0);
    }

    public Agent(String name, String type, List<String> listContraintes) {
        commonInit(name, type, listContraintes, 0);
    }

    private Agent(String name, String type, int nbJoursPlanning) {

        commonInit(name, type, new ArrayList<String>(), nbJoursPlanning);
    }

    //Utilisé pour le vrai agent qui est capable de recevoir et envoyer des messages
    public Agent(String name, String type, int nbJoursPlanning, RabbitMQServer server) {
        this(name, type, nbJoursPlanning);

        if (server != null) {
            this.server = server;
            server.addQueue(name);
            listener = new RabbitMQListenerThread(server, name, new AgentRabbitCallback(this));
            listener.start();
        }
    }

    ///////////
    //ACCESSORS
    ///////////
    public boolean isLocal() {
        return server != null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setListContraintes(List<String> listContraintes) {
        this.listContraintes = listContraintes;
    }

    public void stopListener() {
        if (listener != null) {
            listener.interrupt();
        }
    }

    public int getNumberOfHourWorked() {
        return numberOfHourWorked;
    }

    public void setNumberOfHourWorked(int numberOfHourWorked) {
        this.numberOfHourWorked = numberOfHourWorked;
    }

    public void setType(String newTypeAgent) {
        type = newTypeAgent;
    }

    public List<String> getListContraintes() {
        return listContraintes;
    }

    public int[] getCptGardeSemaine() {
        return cptGardeSemaine;
    }

    public int[] getCptGardeSuite() {
        return cptGardeSuite;
    }

    public int getCptGardeSuite(int idxJour) {
        return cptGardeSuite[idxJour];
    }

    public int getCptGardeSemaine(int idxSemaine) {

        return cptGardeSemaine[idxSemaine];
    }

    public String getType() {
        return type;
    }

    public boolean testGuardeSuite(int jour) {

        if (cptGardeSuite[jour] != 0)//if already working this day
            return false;
        if (cptGardeSemaine[jour / Main.NB_JOURS_PAR_SEMAINES] > Main.MAX_GARDES_PAR_SEMAINE - 1)
            return false;
        int lastCount = 0;
        if (jour > 0 && cptGardeSuite[jour - 1] > Main.MAX_GARDES_SUITE - 1)
            return false;
        else if (jour > 0)
            lastCount = cptGardeSuite[jour - 1] + 1;//used to test if we insert a day between two or more working times
        int i = 1;
        while (jour + i < cptGardeSuite.length && cptGardeSuite[jour + i] != 0) {
            if (cptGardeSuite[jour + i] + lastCount + i > Main.MAX_GARDES_SUITE)
                return false;
            i++;
        }
        return true;
    }

    public void updateWorkOnDay(int jour) {
        cptGardeSemaine[jour / Main.NB_JOURS_PAR_SEMAINES]++;
        if (jour > 0)
            cptGardeSuite[jour] = cptGardeSuite[jour - 1] + 1;//mise à jour du nombre de gardes à la suite
        else
            cptGardeSuite[jour] = 1;
        int i = 1;
        while (jour + i < cptGardeSuite.length && cptGardeSuite[jour + i] != 0) {
            cptGardeSuite[jour + i] += cptGardeSuite[jour + i - 1];
            i++;
        }
        numberOfHourWorked += Main.TEMPS_PAR_GARDES;
    }

    /////////////////////////
    //RABBIT MQ SERVER STUFF
    /////////////////////////
    public void messageReceived(String topic, Message message) {

        if (message.getType().equals(MessageAskForResolveConflict.type)) {
            MessageAskForResolveConflict realMessage = (MessageAskForResolveConflict) message;
            if (realMessage.getAgentName().equals(name)) {
                Planning planning = realMessage.getPlanning();
                cptGardeSemaine = realMessage.getCptGardeSemaine();
                cptGardeSuite = realMessage.getCptGardeSuite();

                List<Integer> conflictsDays = realMessage.getConflictsDays();
                //tri par ordre de préférence
                Collections.sort(conflictsDays,  new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        String j1 = listContraintes.get(o1/2);
                        String j2 = listContraintes.get(o2/2);
                        Integer priorityJ1 = getPriority(o1,j1), priorityJ2 = getPriority(o2,j2);
                        return priorityJ1.compareTo(priorityJ2);
                    }

                    private int getPriority(int index, String j)
                    {
                        int toReturn = 4;
                        if(index%2 == 0 && j.equals("J") || index%2 == 1 && j.equals("N"))
                            toReturn = 0;
                        else if(index%2 == 0 && j.equals("/N") || index%2 == 1 && j.equals("/J") || j.equals("-"))
                            toReturn = 1;
                        else if(index%2 == 0 && j.equals("N") || index%2 == 1 && j.equals("J"))
                            toReturn = 2;
                        else if(j.equals("i"))
                            toReturn = 3;
                        return toReturn;
                    }
                });
                //on teste chaque jour s'il peut
                boolean dayFinded = false;
                for(Integer idxJour : conflictsDays)
                {
                    if((idxJour%2 == 0 && planning.canWorkDay(idxJour/2,this) &&  !listContraintes.get(idxJour/2).equals("/J")
                            || idxJour%2 == 1 && planning.canWorkNight(idxJour/2,this) && !listContraintes.get(idxJour/2).equals("/N"))
                            && !listContraintes.get(idxJour/2).equals("I"))
                    {
                        server.SendMessage(type,new MessageResponseResolveConflict(name,type,idxJour));
                        dayFinded = true;
                        break;
                    }
                }
                if(!dayFinded)
                {
                    server.SendMessage(type,new MessageResponseResolveConflict(name,type));
                }
            } else {
                System.err.println("Message send to Wrong agent. intended Receiver : " + realMessage.getAgentName() + " Real receiver : " + name);
            }
        } else {
            System.err.println("Error Class Agent Method MessageReceived : Unknow message type : " + message.getType());
        }
    }

    public void sendPlanning() {
        if (server != null) {
            Message message = new MessagePlanningContrainteAgent(this);
            server.SendMessage(type, message);
        }
    }

    public String getContrainteForADay(int numDay) {
        return listContraintes.get(numDay);
    }

    public String toString() {
        return name;
    }
}
