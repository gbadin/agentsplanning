package fr.Agent.Smith;

import java.util.*;

public class ResponsablePlanning {

    private ArrayList<Agent> listAgentsPM;
    private ArrayList<Agent> listAgentsAS;
    private Planning p = new Planning();
    private RabbitMQServer server;

    private boolean planningInited;

    private MessageResponseResolveConflict lastMessage;

    public ResponsablePlanning() {
        listAgentsPM = new ArrayList<Agent>(Main.NOMBRE_PERSONEL_MEDICAL);
        listAgentsAS = new ArrayList<Agent>(Main.NOMBRE_ASSISTANT_SOINS);
        planningInited = false;
        lastMessage = null;
    }

    public ResponsablePlanning(RabbitMQServer server) {
        this();
        this.server = server;
    }

    public void messageReceived(String topic, Message message) {

        if (message.getType().equals(MessagePlanningContrainteAgent.type)) {
            MessagePlanningContrainteAgent concreteMessage = (MessagePlanningContrainteAgent) message;
            Agent agent = concreteMessage.getAgent();

            System.out.println("Receive message Planning Contraite on Topic " + topic);

            if (agent.getType().equals("AS"))
                listAgentsAS.add(agent);
            else if (agent.getType().equals("PM"))
                listAgentsPM.add(agent);

            server.addQueue(agent.getName());//ajout du canal de retour de communication

            //quand on a les réponses de tout les agents
            if (listAgentsPM.size() == Main.NOMBRE_PERSONEL_MEDICAL && listAgentsAS.size() == Main.NOMBRE_ASSISTANT_SOINS) {
                planningInited = true;
                System.out.println("Starting Creating Planning");
                organizePlanning();
            }
        } else if (message.getType().equals(MessageResponseResolveConflict.type)) {
            System.out.println("Receive message ResolveConflict on Topic " + topic);
            lastMessage = (MessageResponseResolveConflict) message;
        } else {
            System.err.println("Error Class ResponsablePlanning Method MessageReceived : Unknown message type : " + message.getType());
        }
    }

    public void organizePlanning() {
        TreeMap<Integer, List<Integer>> orderDayPMCanToOrganize = new TreeMap<Integer, List<Integer>>();
        //ArrayList<Integer> orderDayPMWantToOrganize = new ArrayList<Integer>();
        TreeMap<Integer, List<Integer>> orderDayASCanToOrganize = new TreeMap<Integer, List<Integer>>();
        //ArrayList<Integer> orderDayASWantToOrganize = new ArrayList<Integer>();
        getOrderOfTheDayToOrganize(p.getNbSemainesTotal(), p.getNbDaysPerWeek(), listAgentsPM, listAgentsAS,
                orderDayPMCanToOrganize, orderDayASCanToOrganize);

        //étape 1, on regarde qui VEUT faire tel jour
        // verif avant pour les gardes
        //étape 2, on regarde qui PEUT faire tel jour

        createPlanningForType(listAgentsPM, orderDayPMCanToOrganize, Main.NB_PM_PAR_JOUR, Main.NB_PM_PAR_NUIT);

        createPlanningForType(listAgentsAS, orderDayASCanToOrganize, Main.NB_AS_PAR_JOUR, Main.NB_AS_PAR_NUIT);

        System.out.println("Planning before resolving conflicts : \n"+p);

        List<Integer> joursNonCompletPM = getDaysToFillByType(p, "PM");
        List<Integer> joursNonCompletAS = getDaysToFillByType(p, "AS");

        System.out.println(""+(joursNonCompletPM.size()+joursNonCompletAS.size())+ " days that missing someone.");
        System.out.println(""+joursNonCompletPM.size()+ " for PM.");
        System.out.println(""+joursNonCompletAS.size()+ " for AS.");

        boolean planning = resolveConflicts(joursNonCompletPM, listAgentsPM, "PM");
        planning &= resolveConflicts(joursNonCompletAS, listAgentsAS, "AS");

        if(planning)
            System.out.println("Planning successfully created :");
        else
            System.out.println("Planning UnSuccessfully created, Best Planning that have been get :");

        System.out.println(p);

        System.out.println(""+(joursNonCompletPM.size()+joursNonCompletAS.size())+ " days that missing someone.");
        System.out.println(""+joursNonCompletPM.size()+ " for PM.");
        System.out.println(""+joursNonCompletAS.size()+ " for AS.");
    }

    //Résolution des "conflits" (quand il y a des jours où il manque des personnes
    private boolean resolveConflicts(List<Integer> ListeJoursNonComplet, List<Agent> listAgents, String type) {
        // on regarde la liste des jours non complets d'agents pm et d'agents as
        // tant que les 2 listes ne sont pas à 0 :
        // on boucle en envoyant le planning à l'agent qui fait le moins d'heure
        // on attends la réponse et on passe au cas suivant qui peut être :
        //      - L'agent ne peut définitivement pas on passe au suivant dans la liste
        //      - L'agent peut, on enlève le jour de la liste on rajoute l'agent dans le planning et on retrie le tableau et on recommence
        // s'arrète lorsque tout les jours ont été remplis où lorsque tout le monde à répondu négatif (echec)

        sortAgentsByWorkedHours(listAgents);

        int index = 0;
        while (ListeJoursNonComplet.size() != 0 && listAgents.size() > index) {

            Agent a = listAgents.get(index);

            server.SendMessage(a.getName(), new MessageAskForResolveConflict(p, ListeJoursNonComplet, a.getCptGardeSuite(), a.getCptGardeSemaine(), a.getName()));

            while (lastMessage == null) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (lastMessage.canResolve()) {
                int indexJour = lastMessage.getDayAdded();
                a.updateWorkOnDay(indexJour / 2);

                if (indexJour % 2 == 0) {
                    p.getJour(indexJour / 2).getJournee().getListAgents().add(a);
                    if(type.equals("AS")  &&  p.getJour(indexJour / 2).getJournee().hasSufficientAS()|| type.equals("PM") && p.getJour(indexJour / 2).getJournee().hasSufficientPM())
                    {
                        ListeJoursNonComplet.remove((Integer)indexJour);
                    }
                } else {
                    p.getJour(indexJour / 2).getNuit().getListAgents().add(a);
                    if(type.equals("AS")  &&  p.getJour(indexJour / 2).getNuit().hasSufficientAS()|| type.equals("PM") && p.getJour(indexJour / 2).getNuit().hasSufficientPM())
                    {
                        ListeJoursNonComplet.remove((Integer)indexJour);
                    }
                }

                sortAgentsByWorkedHours(listAgents);
            } else {
                index++;
            }
            lastMessage = null;
        }
        return ListeJoursNonComplet.size() == 0;//on vérifie si on a réussis à tout trier !
    }

    public void organizePlanning(ArrayList<Agent> listAgentsPM, ArrayList<Agent> listAgentsAS) {
        this.listAgentsAS = listAgentsAS;
        this.listAgentsPM = listAgentsPM;

        organizePlanning();
    }

    private void createPlanningForType(ArrayList<Agent> listAgents, TreeMap<Integer, List<Integer>> orderDayCanToOrganize, int numberPeopleDay, int numberPeopleNight) {
        //parcours de la liste des jours pour les PM
        for (Map.Entry<Integer, List<Integer>> entry : orderDayCanToOrganize.entrySet()) {
            for (Integer indexJour : entry.getValue()) {
                Jour j = p.getJour((int) Math.floor(indexJour / 2));
                Journee jo = j.getJournee();
                Nuit n = j.getNuit();
                List<Agent> listAgentsJour = jo.getListAgents();
                List<Agent> listAgentsNuit = n.getListAgents();
                List<Agent> listAgentsPossible = new ArrayList<Agent>();

                //étape 1 : on regarde l'index du jour dans l'entry
                //étape 2 : on cherche un pm qui veut ce jour (avec "J" et "N")
                //étape 3 : on affecte le pm au jour de l'index

                //c'est la journee
                if (indexJour % 2 == 0) {
                    setListAgentForTimeSlot(listAgents, numberPeopleDay, indexJour, listAgentsPossible, listAgentsJour, true);
                } else {
                    //c'est la nuit
                    setListAgentForTimeSlot(listAgents, numberPeopleNight, indexJour, listAgentsPossible, listAgentsNuit, false);
                }
            }
        }
    }

    private void setListAgentForTimeSlot(List<Agent> listAgents, int numberPeople, Integer indexJour, List<Agent> listAgentsPossible, List<Agent> listAgentsToSet, boolean isDay) {
        //répartition des Agents
        int cptAgentsJour = 0;
        for (Agent a : listAgents) {
            if ((isDay && a.getContrainteForADay(indexJour / 2).equals("J") && p.canWorkDay(indexJour / 2, a))
                    || (!isDay && a.getContrainteForADay(indexJour / 2).equals("N") && p.canWorkNight(indexJour / 2, a))
                    ) {
                //ajout de l'agent à la liste d'agents possible de la journee
                listAgentsPossible.add(a);
            }
        }

        //Sélectionne les agents avec le moins d'heures effectuées
        sortAgentsByWorkedHours(listAgentsPossible);
        for (Agent a : listAgentsPossible) {
            if (cptAgentsJour >= numberPeople)
                break;
            cptAgentsJour++;
            listAgentsToSet.add(a);
            a.updateWorkOnDay(indexJour / 2);//set everything we need ;)
        }

        if (cptAgentsJour < numberPeople) //cas où il n'y a pas assez d'agents qui veulent et il faut en rajouter
        {
            listAgentsPossible.clear();
            for (Agent a : listAgents) {
                if (a.getContrainteForADay(indexJour / 2).equals("-")
                        || (isDay && a.getContrainteForADay(indexJour / 2).equals("/N") && p.canWorkDay(indexJour / 2, a))
                        || (!isDay && a.getContrainteForADay(indexJour / 2).equals("/J") && p.canWorkNight(indexJour / 2, a))
                        ) {
                    //ajout de l'agent à la liste d'agents possible
                    listAgentsPossible.add(a);
                }
            }

            //Sélectionne les agents avec le moins d'heures effectuées
            sortAgentsByWorkedHours(listAgentsPossible);
            for (Agent a : listAgentsPossible) {
                if (cptAgentsJour >= numberPeople)
                    break;
                cptAgentsJour++;
                listAgentsToSet.add(a);
                a.updateWorkOnDay(indexJour / 2);//set everything we need ;)
            }
        }
    }

    private void sortAgentsByWorkedHours(List<Agent> list) {
        Collections.sort(list, new Comparator<Agent>() {
            @Override
            public int compare(Agent a1, Agent a2) {
                return Integer.compare(a1.getNumberOfHourWorked(), a2.getNumberOfHourWorked());
            }
        });
    }

    //on récupère une map avec en clé le nombre de possibilités et en valeur une liste des jours qui ont ce nombre de possibilités
    public void getOrderOfTheDayToOrganize(int numWeek, int numDaysPerWeek, ArrayList<Agent> listAgentsPM, ArrayList<Agent> listAgentsAS,
                                           Map<Integer, List<Integer>> orderDayPMCanToOrganize, Map<Integer, List<Integer>> orderDayASCanToOrganize) {
        //String[] listSymbolePlanning = {"J","N","/J","/N","-","i","I","v"};
        //récupération du nombre des agents disponible des agents par jour de la semaine
        for (int i = 0; i < numWeek * numDaysPerWeek; i++) {

            int cptAgentPMJourPeut = 0;
            int cptAgentPMNuitPeut = 0;
            //int cptAgentPMVeut = 0;
            int cptAgentASJourPeut = 0;
            int cptAgentASNuitPeut = 0;
            //int cptAgentASVeut = 0;

            //compteur agents pm
            for (Agent agent : listAgentsPM) {
                String dispo = agent.getContrainteForADay(i);
                if (dispo.equals("-")) {
                    cptAgentPMJourPeut++;
                    cptAgentPMNuitPeut++;
                } else if (dispo.equals("J") || dispo.equals("/N"))
                    cptAgentPMJourPeut++;
                else if (dispo.equals("N") || dispo.equals("/J")) {
                    cptAgentPMNuitPeut++;
                    //cptAgentPMVeut++;
                }
            }

            //compteur agents as
            for (Agent agent : listAgentsAS) {
                String dispo = agent.getContrainteForADay(i);
                if (dispo.equals("-")) {
                    cptAgentASJourPeut++;
                    cptAgentASNuitPeut++;
                } else if (dispo.equals("J") || dispo.equals("/N"))
                    cptAgentASJourPeut++;
                else if (dispo.equals("N") || dispo.equals("/J")) {
                    cptAgentASNuitPeut++;
                    //cptAgentPMVeut++;
                }
            }

            //Si l'entrée dans la map n'existe pas
            if (!orderDayPMCanToOrganize.containsKey(cptAgentPMJourPeut))
                orderDayPMCanToOrganize.put(cptAgentPMJourPeut, new ArrayList<Integer>());
            if (!orderDayPMCanToOrganize.containsKey(cptAgentPMNuitPeut))
                orderDayPMCanToOrganize.put(cptAgentPMNuitPeut, new ArrayList<Integer>());
            if (!orderDayASCanToOrganize.containsKey(cptAgentASJourPeut))
                orderDayASCanToOrganize.put(cptAgentASJourPeut, new ArrayList<Integer>());
            if (!orderDayASCanToOrganize.containsKey(cptAgentASNuitPeut))
                orderDayASCanToOrganize.put(cptAgentASNuitPeut, new ArrayList<Integer>());

            orderDayPMCanToOrganize.get(cptAgentPMJourPeut).add(i * 2);
            orderDayPMCanToOrganize.get(cptAgentPMNuitPeut).add(i * 2 + 1);
            orderDayASCanToOrganize.get(cptAgentASJourPeut).add(i * 2);
            orderDayASCanToOrganize.get(cptAgentASNuitPeut).add(i * 2 + 1);
        }
    }

    // retourne une liste de jours à remplir en fonction du type de l'agent
    List<Integer> getDaysToFillByType(Planning p, String typeAgent) {
        int cptAgentJournee;
        int cptAgentNuit;
        List<Integer> listDaysToFill = new ArrayList<Integer>();

        for (int i = 0; i < p.getNbJoursTotal(); i++) {
            Jour j = p.getJour(i);
            Journee jo = j.getJournee();
            Nuit n = j.getNuit();

            if (typeAgent.equals("PM")) {
                if (!jo.hasSufficientPM())
                    listDaysToFill.add(i*2);
                if(!n.hasSufficientPM())
                    listDaysToFill.add(i*2+1);
            } else if (typeAgent.equals("AS")) {
                if (!jo.hasSufficientAS())
                    listDaysToFill.add(i*2);
                if(!n.hasSufficientAS())
                    listDaysToFill.add(i*2+1);
            }
        }
        return listDaysToFill;
    }

    /*public boolean hasDonePreviousNight(Agent a, int indexJour){
        if(indexJour > 0)
        {
           Jour j = p.getJour((int)Math.floor((indexJour-2)/2));
            Nuit n = j.getNuit();
            List<Agent> listAgentsNuit = n.getListAgents();
            if(!listAgentsNuit.contains(a))
                return true;
        }
        return false;
    }

    public boolean hasDonePreviousDay(Agent a, int indexNuit){

        Jour j = p.getJour((int)Math.floor((indexNuit)/2));
        Journee jo = j.getJournee();
        List<Agent> listAgentsJournee = jo.getListAgents();
        if(!listAgentsJournee.contains(a))
            return true;

        return false;
    }
    public int compteGardeSuite(Agent a, int idxSemaine)
    {
        int cptJour = 0;
        int cptNuit = 0;
        boolean suiteJournee = false;
        boolean suiteNuit = false;
        for(int i = 0; i < p.getNbJoursTotal(); i++)
        {
            Jour j = p.getJour(i);
            Journee jo = j.getJournee();
            Nuit n = j.getNuit();
            List<Agent> listAgentsJour = jo.getListAgents();
            List<Agent> listAgentsNuit = n.getListAgents();

            if(listAgentsJour.contains(a) && !suiteJournee)
            {
                suiteJournee = true;
                cptJour = 0;
                cptJour ++;
            }
            else if(listAgentsJour.contains(a) && suiteJournee)
                cptJour++;

            if(listAgentsNuit.contains(a) && !suiteNuit)
            {
                suiteNuit = true;
                cptNuit = 0;
                cptNuit ++;
            }
            else if(listAgentsNuit.contains(a) && suiteNuit)
                cptNuit++;
        }


        return Math.max(cptJour, cptNuit);
    }*/
}


