package fr.Agent.Smith;

public class ResponsableRabbitCallback implements IRabbitCallback {

    private final ResponsablePlanning responsable;

    public ResponsableRabbitCallback(ResponsablePlanning responsable) {
        this.responsable = responsable;
    }

    @Override
    public void messageReceived(String topic, Message message) {
        responsable.messageReceived(topic, message);
    }
}
