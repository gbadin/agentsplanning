package fr.Agent.Smith;

import java.util.ArrayList;

public class Main {

    public static final int NOMBRE_PERSONEL_MEDICAL = 40;
    public static final int NOMBRE_ASSISTANT_SOINS = 10;

    public static final int NB_SEMAINES = 4;
    public static final int NB_JOURS_PAR_SEMAINES = 7;
    public static final int NB_GARDES_PAR_JOUR = 2;
    public static final int NB_PM_PAR_JOUR = 3;
    public static final int NB_AS_PAR_JOUR = 1;
    public static final int NB_PM_PAR_NUIT = 1;
    public static final int NB_AS_PAR_NUIT = 1;
    public static final int TEMPS_HEURES_MOYENNE_PM_PAR_SEMAINE = 40;
    public static final int TEMPS_PAR_GARDES = 12;

    public static final int MAX_GARDES_SUITE = 3;
    public static final int MAX_GARDES_PAR_SEMAINE = 4;

    private static void testToStringPlanning() {
        Planning p = new Planning();
        p.getSemaine(0).getJour(3).getNuit().getListAgents().add(new Agent("AM1", "AM", null));
        System.out.println(p);
    }

    private static void testRabbit() {
        RabbitMQServer server = new RabbitMQServer();
        ResponsablePlanning responsable = new ResponsablePlanning(server);
        RabbitMQListener listener = new RabbitMQListener(server, responsable, RabbitMQServer.QUEUES_NAME);
        listener.start();

        server.SendMessage("AM", "Yop !");
        server.SendMessage("PM", "Gnialut :3");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        listener.stop();
        server.close();
    }

    private static void testResponsable() {
        RabbitMQServer server = new RabbitMQServer();
        ResponsablePlanning responsable = new ResponsablePlanning(server);
        RabbitMQListener listener = new RabbitMQListener(server, responsable, RabbitMQServer.QUEUES_NAME);
        listener.start();

        Planning p = new Planning();
        ArrayList<Agent> listAgentsPM = new ArrayList<Agent>();
        ArrayList<Agent> listAgentsAS = new ArrayList<Agent>();

        for (int i = 0; i < NOMBRE_PERSONEL_MEDICAL; i++) {
            Agent a = new Agent("PM" + i, "PM", p.getNbJoursTotal(), server);
            listAgentsPM.add(a);
        }

        for (int i = 0; i < NOMBRE_ASSISTANT_SOINS; i++) {
            Agent a = new Agent("AS" + i, "AS", p.getNbJoursTotal(), server);
            listAgentsAS.add(a);
        }

        for (Agent a : listAgentsPM) {
            a.sendPlanning();
        }
        for (Agent a : listAgentsAS) {
            a.sendPlanning();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (Agent a : listAgentsPM) {
            a.stopListener();
        }
        for (Agent a : listAgentsAS) {
            a.stopListener();
        }

        listener.stop();
        server.close();
    }

    public static void testOrganizePlanning() {
        Planning p = new Planning();
        ArrayList<Agent> listAgentsPM = new ArrayList<Agent>();
        ArrayList<Agent> listAgentsAS = new ArrayList<Agent>();
        ResponsablePlanning r = new ResponsablePlanning();

        for (int i = 0; i < NOMBRE_PERSONEL_MEDICAL; i++) {
            Agent a = new Agent("PM" + i, "PM", p.getNbJoursTotal(), null);
            listAgentsPM.add(a);
        }

        for (int i = 0; i < NOMBRE_ASSISTANT_SOINS; i++) {
            Agent a = new Agent("AS" + i, "AS", p.getNbJoursTotal(), null);
            listAgentsAS.add(a);
        }
        r.organizePlanning(listAgentsPM, listAgentsAS);
    }

    public static void main(String[] args) {
        testResponsable();
    }
}
