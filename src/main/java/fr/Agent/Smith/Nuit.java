package fr.Agent.Smith;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Nuit implements Serializable {

    private transient List<Agent> listAgent;

    public Nuit() {
        listAgent = new ArrayList<Agent>();
    }

    public List<Agent> getListAgents() {
        return listAgent;
    }

    public String toString() {

        String res = "";

        if (listAgent != null && listAgent.size() > 0) {
            for (Agent a : listAgent) {
                res += a + ",";
            }

            res = res.substring(0, res.length() - 1);
        }

        return res;
    }

    public boolean hasSufficientAS() {
        int count = 0;
        for (Agent a : listAgent)
        {
            if(a.getType().equals("AS"))
                count ++;
        }
        return count == Main.NB_AS_PAR_NUIT;
    }

    public boolean hasSufficientPM() {
        int count = 0;
        for (Agent a : listAgent)
        {
            if(a.getType().equals("PM"))
                count ++;
        }
        return count == Main.NB_PM_PAR_NUIT;
    }

    ///////////////
    //SERIALIZATION
    ///////////////
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.writeInt(listAgent.size());
        for (Agent a : listAgent) {
            s.writeObject(a.getName());
            s.writeObject(a.getType());
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        int size = s.readInt();
        listAgent = new ArrayList<Agent>();
        for (int i = 0; i < size; i++) {
            String agentName = (String) s.readObject();
            String agentType = (String) s.readObject();
            if (Environment.getEnvironment().containsAgent(agentName)) {
                listAgent.add(Environment.getEnvironment().getAgent(agentName));
            } else {
                listAgent.add(new Agent(agentName, agentType));
            }
        }
    }
}
