package fr.Agent.Smith;

public class AgentRabbitCallback implements IRabbitCallback {

    private final Agent agent;

    public AgentRabbitCallback(Agent agent) {
        this.agent = agent;
    }

    @Override
    public void messageReceived(String topic, Message message) {
        agent.messageReceived(topic, message);
    }
}
